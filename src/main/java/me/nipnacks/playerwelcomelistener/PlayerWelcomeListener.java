package me.nipnacks.playerwelcomelistener;

import me.nipnacks.playerwelcomelistener.commands.WelcomeCommand;
import me.nipnacks.playerwelcomelistener.events.Events;
import me.nipnacks.playerwelcomelistener.welcome.WelcomeManager;
import org.bukkit.plugin.java.JavaPlugin;

public final class PlayerWelcomeListener extends JavaPlugin {

    @Override
    public void onEnable() {
        // Plugin startup logic
        System.out.println("PlayerWelcomeListener is now starting up");
        System.out.println("Made by NipNacks");

        getConfig().options().copyDefaults();
        saveDefaultConfig();
        WelcomeManager welcomeManager = new WelcomeManager(this);
        getServer().getPluginManager().registerEvents(new Events(this, welcomeManager), this);
        this.getCommand("welcome").setExecutor(new WelcomeCommand(this));

    }

    @Override
    public void onDisable() {
        // Plugin shutdown logic
    }
}
