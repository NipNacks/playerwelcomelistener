package me.nipnacks.playerwelcomelistener.commands;

import me.nipnacks.playerwelcomelistener.PlayerWelcomeListener;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;

public class WelcomeCommand implements CommandExecutor {

    private PlayerWelcomeListener plugin;

    public WelcomeCommand(PlayerWelcomeListener plugin) {
        this.plugin = plugin;
    }

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        String version = "1.0";
        String author = "NipNacks";

        if (args.length < 1) {
            sender.sendMessage(ChatColor.DARK_GRAY + "» " + ChatColor.AQUA + "Welcome " + ChatColor.GRAY
                    + ChatColor.GRAY + "v" + version);
            if (sender.hasPermission("welcome.reload")) {
                sender.sendMessage(ChatColor.GRAY + "/welcome reload " + ChatColor.DARK_GRAY + "-" + ChatColor.RESET
                        + " Reload plugin");
            }
            sender.sendMessage(ChatColor.DARK_GRAY + "Author: " + ChatColor.GRAY + author);
            return true;
        }
        if (args.length == 1) {
            if (args[0].toLowerCase().equals("reload")) {
                if (sender.hasPermission("welcome.reload")) {
                    plugin.reloadConfig();
                    sender.sendMessage(ChatColor.DARK_GRAY + "[" + ChatColor.AQUA + "Welcome"
                            + ChatColor.DARK_GRAY + "]" + ChatColor.RESET + " Config reloaded!");
                } else {
                    sender.sendMessage(ChatColor.DARK_GRAY + "[" + ChatColor.AQUA + "Welcome"
                            + ChatColor.DARK_GRAY + "]" + ChatColor.RESET + " No Permission!");
                }
            }
        }
        return true;
    }
}
