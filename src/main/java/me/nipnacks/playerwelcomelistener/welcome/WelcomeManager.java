package me.nipnacks.playerwelcomelistener.welcome;

import me.nipnacks.playerwelcomelistener.PlayerWelcomeListener;
import org.bukkit.entity.Player;

import java.util.List;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

public class WelcomeManager {

    private PlayerWelcomeListener plugin;
    private ConcurrentMap<Player, NewPlayer> newPlayers = new ConcurrentHashMap<>();

    public WelcomeManager(PlayerWelcomeListener plugin) {
        this.plugin = plugin;
    }

    public ConcurrentMap<Player, NewPlayer> getNewPlayers() {
        return newPlayers;
    }

    public void addNew(Player player) {
        newPlayers.put(player, new NewPlayer(player));
    }

    public void removeNew(Player player) {
        newPlayers.remove(player);
    }

    public Boolean messageContains(String message) {
        List<String> welcomeText = plugin.getConfig().getStringList("settings.welcomeText");

        for (String string : welcomeText) {
            if (message.toLowerCase().contains(string.toLowerCase())) {
                return true;
            }
        }

        return false;
    }
}

